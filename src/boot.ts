import * as dotenv from 'dotenv';
import * as tsConfigPaths from 'tsconfig-paths';
import { compilerOptions } from '../tsconfig.json';

tsConfigPaths.register({
  baseUrl: compilerOptions.baseUrl,
  paths: {},
});

dotenv.config();

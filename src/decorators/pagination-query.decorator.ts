// types
import { Request } from 'types';
import { IListQuery } from 'types/model';

export function paginationQuery(target: any, property: string) {
  const currentMethod = target[property];

  target[property] = async (req: Request, res: any) => {
    const {
      next,
      limit,
      previous,
      search,
      sort,
      totalCount,
    } = req.query as IListQuery;

    const paginationData: IListQuery = JSON.parse(
      JSON.stringify({
        next,
        limit,
        previous,
        search,
        sort,
        totalCount,
      }),
    );

    req.paginationData = paginationData;

    return await currentMethod(req, res);
  };
}

// types
import { FastifyInstance, HTTPMethods, FastifyPluginOptions } from 'fastify';

export function route(prefix?: string) {
  if (!prefix) {
    prefix = '/';
  }

  return (target: any) => {
    target._prefix = prefix;
  };
}

function all(path: string, method: HTTPMethods) {
  return (target: any, name: string) => {
    target[name]._routeData = {
      method,
      url: path,
      handler: target[name],
    };
  };
}

export function schema(schemaData: object) {
  return (target: any, name: string) => {
    target[name]._schema = schemaData;
  };
}

export function get(path: string) {
  return all(path, 'GET');
}

export function post(path: string) {
  return all(path, 'POST');
}

export function put(path: string) {
  return all(path, 'PUT');
}

export function del(path: string) {
  return all(path, 'DELETE');
}

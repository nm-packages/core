export default class NotBootError extends Error {
  public statusCode: number = 400;

  constructor(name: string) {
    super(`${name} is not booted yet`);
  }
}

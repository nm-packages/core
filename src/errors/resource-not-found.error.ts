export default class ResourceNotFoundError extends Error {
  public statusCode: number = 400;

  constructor(id: string, collection: string) {
    super(
      `Resource with collection name '${collection}' and id '${id}' is not found!`,
    );
  }
}

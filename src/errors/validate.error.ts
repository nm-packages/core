export default class ValidateError extends Error {
  public statusCode: number = 400;
}

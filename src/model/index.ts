import BaseModel from 'model/model.base';
// types
import { IExtraListQuery, IList } from 'types/model';

export default abstract class ExtraBaseModel<T> extends BaseModel<T> {
  public async list(
    filterData: any = {},
    listQuery: IExtraListQuery = {},
  ): Promise<IList<T>> {
    const pipelines: object[] = [];

    listQuery.limit = listQuery.limit || BaseModel.maxLimit;
    const data = this.setUpPagination(listQuery, pipelines);
    this.setUpSorting(listQuery, pipelines);
    this.setUpSearch(filterData, listQuery);

    listQuery.extraPipelines = [
      ...pipelines,
      ...(listQuery.extraPipelines || []),
    ];

    const promises: any[] = [super.list(filterData, listQuery)];

    if (listQuery.totalCount) {
      promises.push(this.collection.countDocuments());
    }

    const [result, totalCount] = await Promise.all(promises);

    if (typeof totalCount === 'number') {
      result.totalCount = totalCount;
    }

    this.setUpPaginationResult(result, {
      ...data,
      limit: listQuery.limit,
    });

    return result;
  }

  // private

  private setUpPagination(
    listQuery: IExtraListQuery = {},
    pipelines: object[],
  ) {
    if (listQuery.limit === undefined) {
      throw new Error('Pagination limit is not set up');
    }

    if (listQuery.next && listQuery.previous) {
      throw new Error('Pagination can has only one field - next or previous');
    } else if (
      listQuery.next === undefined &&
      listQuery.previous === undefined
    ) {
      listQuery.next = 0;
    }

    const extraListData: any = {};
    const data: any = {};

    if (typeof listQuery.next === 'number') {
      extraListData.next = listQuery.next;
      data.next = listQuery.next + listQuery.limit;

      const previous = listQuery.next - listQuery.limit;

      if (previous >= 0) {
        data.previous = previous;
      }

      pipelines.push({
        $skip: extraListData.next,
      });
    } else if (typeof listQuery.previous === 'number') {
      extraListData.previous = listQuery.previous;
      data.next = listQuery.previous + listQuery.limit;

      const previous = listQuery.previous - listQuery.limit;

      if (previous >= 0) {
        data.previous = previous;
      }

      if (extraListData.previous < 0) {
        extraListData.previous = 0;
      } else {
        pipelines.push({
          $skip: extraListData.previous,
        });
      }
    }

    pipelines.push({
      $limit: listQuery.limit + 1,
    });

    return data;
  }

  private setUpPaginationResult(result: IList<T>, data: any) {
    result.next = typeof data.next === 'number' ? data.next : null;
    result.previous = typeof data.previous === 'number' ? data.previous : null;
    result.limit = data.limit as number;

    if (result.data.length > result.limit) {
      result.hasNext = true;
      result.data.pop();
    } else {
      result.hasNext = false;
      result.next = null;
    }

    if (typeof result.next === 'number') {
      const currentPosition = result.next - result.limit;

      if (currentPosition > 0) {
        result.hasPrevious = true;
      } else if (currentPosition === 0) {
        result.hasPrevious = false;
      }
    } else {
      result.hasPrevious = true;
    }
  }

  private setUpSorting(listQuery: IExtraListQuery = {}, pipelines: object[]) {
    const sortData: any = {};

    if (!listQuery.sort) {
      listQuery.sort = {
        asc: [],
        desc: [],
      };
    }

    if (!listQuery.sort.asc.includes('created')) {
      listQuery.sort.asc.push('created');
    }

    listQuery.sort.asc.forEach((v) => {
      sortData[v] = 1;
    });

    listQuery.sort.desc.forEach((v) => {
      sortData[v] = -1;
    });

    if (Object.keys(sortData).length > 0) {
      pipelines.push({
        $sort: sortData,
      });
    }
  }

  private setUpSearch(filterData: any = {}, listQuery: IExtraListQuery = {}) {
    if (!listQuery.search) {
      return;
    }

    for (const key in listQuery.search) {
      filterData[key] = new RegExp(listQuery.search[key].trim(), 'gmi');
    }
  }
}

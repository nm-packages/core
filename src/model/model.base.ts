import { Collection, ObjectId } from 'mongodb';
import { ValidateFunction } from 'ajv';
import Schemes from 'schemes';
import Validator from 'validator';
// providers
import DBProvider from 'providers/db.provider';
// types
import { IList, IExtraListQuery } from 'types/model';
// errors
import NotBootError from 'errors/not-boot.error';
import ValidateError from 'errors/validate.error';
import ResourceNotFoundError from 'errors/resource-not-found.error';

export default abstract class BaseModel<T> {
  static readonly maxLimit = 100;
  protected abstract collectionName: string;
  protected abstract schema: any;

  protected _validator!: ValidateFunction;
  protected _isBooted: boolean = false;
  protected _collection?: Collection<T>;

  public get collection(): Collection<T> {
    if (!this._collection) {
      throw new NotBootError('Model');
    }

    return this._collection;
  }

  // public

  public async boot() {
    if (this._isBooted) {
      return;
    }

    this._collection = DBProvider.db.collection<T>(this.collectionName);

    this.setUpSchema();

    this._validator = Validator(this.schema);

    this._isBooted = true;
  }

  public async validate(data: any): Promise<any> {
    this.setObjectId(data);

    const validated = await this._validator(data);

    if (
      !validated &&
      this._validator.errors &&
      this._validator.errors.length > 0
    ) {
      throw new ValidateError(this._validator.errors[0].message);
    }

    return data;
  }

  public async get(
    id: string | ObjectId,
    throwIfNotFound: boolean = true,
  ): Promise<T> {
    const result = await this.collection.findOne({
      _id: new ObjectId(id),
    } as any);

    if (!result && throwIfNotFound) {
      throw new ResourceNotFoundError(id.toString(), this.collectionName);
    }

    return result as T;
  }

  public async list(
    filterData: any = {},
    listQuery: IExtraListQuery = {},
  ): Promise<IList<T>> {
    this.setObjectId(filterData);

    const list = await this.collection
      .aggregate([
        {
          $match: filterData,
        },
        ...(listQuery.extraPipelines || []),
      ])
      .toArray();

    const result: IList<T> = {
      data: list,
    };

    return result;
  }

  public async create(data: any): Promise<T> {
    data = {
      ...data,
      created: Date.now(),
      updated: Date.now(),
    };

    await this.validate(data);

    const result = await this.collection.insertOne(data);

    return result.ops[0] as T;
  }

  public async update(id: string | ObjectId, data: any): Promise<T> {
    data = {
      ...data,
      updated: Date.now(),
    };

    await this.validate(data);

    const result = await this.collection.findOneAndUpdate(
      {
        _id: new ObjectId(id),
      } as any,
      {
        $set: data,
      },
      {
        returnOriginal: false,
      },
    );

    return result.value as T;
  }

  // private

  protected setUpSchema() {
    this.schema = {
      additionalProperties: false,
      ...this.schema,
    };

    this.schema.properties = {
      ...this.schema.properties,
      _id: Schemes.ObjectID,
      created: Schemes.Number,
      updated: Schemes.Number,
    };

    for (const key in this.schema.properties) {
      if (typeof this.schema.properties[key] === 'function') {
        this.schema.properties[key] = this.schema.properties[key]();
      }
    }
  }

  protected setObjectId(data: any) {
    for (const key in this.schema.properties) {
      if (
        typeof data[key] === 'string' &&
        this.schema.properties[key].objectId
      ) {
        data[key] = new ObjectId(data[key]);
      }
    }
  }
}

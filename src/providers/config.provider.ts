import * as path from 'path';
import { promises as fs } from 'fs';
// types
import { IProvider } from 'types';
import { IAppConfig, IDBConfig, IHttpConfig } from 'types/config';
// errors
import NotBootError from 'errors/not-boot.error';

interface IConfig {
  app: IAppConfig;
  db: IDBConfig;
  http: IHttpConfig;
  [key: string]: any;
}

export class ConfigProvider implements IProvider {
  protected _config?: IConfig;

  public get config(): IConfig {
    if (!this._config) {
      throw new NotBootError('ConfigProvider');
    }

    return this._config;
  }

  public getConfig<T>(): IConfig & T {
    return this.config as any;
  }

  public async boot(configDir: string = path.resolve('src/app/configs')) {
    const config: any = {};

    const files = await fs.readdir(configDir);

    files.forEach((file) => {
      config[path.parse(file).name] = require(path.join(
        configDir,
        file,
      )).default;
    });

    this._config = config;
  }

  public async unboot() {
    this._config = undefined;
  }
}

export default new ConfigProvider();

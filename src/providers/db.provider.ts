import { MongoClient, Db } from 'mongodb';
import * as path from 'path';
import { promises as fs } from 'fs';
// providers
import ConfigProvider from 'providers/config.provider';
// types
import { IProvider } from 'types';
// errors
import NotBootError from 'errors/not-boot.error';

export class DBProvider implements IProvider {
  protected _client?: MongoClient;

  public get client(): MongoClient {
    if (!this._client) {
      throw new NotBootError('DBProvider');
    }

    return this._client;
  }

  protected _db?: Db;

  public get db(): Db {
    if (!this._db) {
      throw new NotBootError('DBProvider');
    }

    return this._db;
  }

  public async ping(): Promise<boolean> {
    const pingMongoResult = await this.db.admin().ping();

    return pingMongoResult && pingMongoResult.ok;
  }

  public async boot({
    connection = ConfigProvider.config.db.connection,
    name = ConfigProvider.config.db.name,
    options = ConfigProvider.config.db.options,
    modelsDir = ConfigProvider.config.app.dirs.models,
  } = {}) {
    this._client = await MongoClient.connect(
      `${connection}/${name}${options || ''}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    );
    this._db = this._client.db(name);
    await this.bootModels(modelsDir);
  }

  public async unboot() {
    await this._client?.close();
    this._client = undefined;
    this._db = undefined;
  }

  protected async bootModels(dirPath: string) {
    const files = await fs.readdir(dirPath);

    await Promise.all(
      files.map(async (file) => {
        const pathToFile = path.join(dirPath, file);

        const stat = await fs.lstat(pathToFile);

        if (stat.isFile()) {
          await require(pathToFile).default.boot();
        }
      }),
    );
  }
}

export default new DBProvider();

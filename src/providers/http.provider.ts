import fastify, { FastifyInstance, FastifyPluginOptions } from 'fastify';
import * as path from 'path';
import { promises as fs } from 'fs';
// providers
import ConfigProvider from 'providers/config.provider';
// types
import { IProvider } from 'types';
// errors
import NotBootError from 'errors/not-boot.error';

export class HttpProvider implements IProvider {
  protected _server?: FastifyInstance;

  public get server(): FastifyInstance {
    if (!this._server) {
      throw new NotBootError('HttpProvider');
    }

    return this._server;
  }

  public get request() {
    return this.server.inject;
  }

  public async ping() {
    const ping = await this.server.inject({
      method: 'GET',
      url: '/ping',
    });

    return ping.body === 'OK';
  }

  public async boot({
    logger = ConfigProvider.config.http.logger,
    port = ConfigProvider.config.http.port,
    controllersDir = ConfigProvider.config.app.dirs.controllers,
  } = {}) {
    this._server = fastify({
      logger,
    });

    await this.bootControllers(controllersDir);

    await this.server.listen(port);
  }

  public async unboot() {
    await this.server.close();
    this._server = undefined;
  }

  protected async bootControllers(dirPath: string) {
    const files = await fs.readdir(dirPath);

    files.forEach((file) => {
      this.initRoutes(require(path.join(dirPath, file)).default);
    });
  }

  protected initRoutes(controller: any) {
    const prefix = controller._prefix || '/';

    for (const key in controller) {
      if (typeof controller[key] === 'function' && controller[key]._routeData) {
        const routeFunction: any = (
          fastifyIns: FastifyInstance,
          opts: FastifyPluginOptions,
          next: () => {},
        ) => {
          fastifyIns.route({
            ...controller[key]._routeData,
            schema: controller[key]._schema,
          });

          next();
        };

        this.server.register(routeFunction, { prefix });
      }
    }
  }
}

export default new HttpProvider();

export function ObjectID() {
  return {
    type: 'object',
    objectId: true,
  };
}

export function String(minLength: number | false = 1) {
  const type: any = {
    type: 'string',
  };

  if (minLength !== false) {
    type.minLength = minLength;
  }

  return type;
}

export function Number(
  minValue: number | false = 0,
  numberType: 'number' | 'integer' = 'number',
) {
  const type: any = {
    type: numberType,
  };

  if (minValue !== false) {
    type.minimum = minValue;
  }

  return type;
}

export function Integer(minValue: number | false = false) {
  return Number(minValue, 'integer');
}

export function Boolean() {
  return {
    type: 'boolean',
  };
}

export function Array(items: any) {
  return {
    type: 'array',
    items: typeof items === 'function' ? items() : items,
  };
}

export function Default(type: any, value: any) {
  const data = typeof type === 'function' ? type() : type;

  data.default = value;

  return data;
}

const Schemas = {
  ObjectID,
  String,
  Number,
  Integer,
  Boolean,
  Array,
};

export default Schemas;

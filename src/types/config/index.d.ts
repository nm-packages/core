export interface IAppConfig {
  name: string;
  dirs: {
    root: string;
    controllers: string;
    models: string;
  };
}

export interface IDBConfig {
  connection: string;
  name: string;
  options?: string;
}

export interface IHttpConfig {
  logger: boolean;
  port: number;
}

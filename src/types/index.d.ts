import { FastifyRequest } from 'fastify';
import { IListQuery } from 'types/model';

export interface IProvider {
  ping?(): Promise<boolean> | boolean;
  boot(...params: any): Promise<void> | void;
  unboot(): Promise<void> | void;
}

export interface Request<Params = any, Query = any, Body = any, Headers = any>
  extends FastifyRequest {
  params: Params;
  query: Query;
  body: Body;
  headers: {} & Headers;
  paginationData: IListQuery;
}

export interface IModel {
  _id: any;
  created: number;
  updated: number;
}

export interface IList<T> {
  data: T[];
  totalCount?: number;
  next?: number | null;
  previous?: number | null;
  hasNext?: boolean;
  hasPrevious?: boolean;
  limit?: number;
}

export interface IListQuery {
  sort?: {
    asc: string[];
    desc: string[];
  };
  search?: {
    [key: string]: string;
  };
  totalCount?: boolean;
  limit?: number;
  next?: number | null;
  previous?: number | null;
}

export interface IExtraListQuery extends IListQuery {
  extraPipelines?: object[];
}

import * as Ajv from 'ajv';

const ajv = new Ajv({
  removeAdditional: true,
  useDefaults: true,
  coerceTypes: true,
  allErrors: true,
  nullable: true,
});

export default function compiler(schema: any): Ajv.ValidateFunction {
  return ajv.compile(schema);
}

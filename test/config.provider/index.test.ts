import { expect } from 'chai';
import * as path from 'path';
import appConfig from './configs/app';
// provider
import ConfigProvider from 'providers/config.provider';

describe('Config Provider', () => {
  it('load and check config data', async () => {
    await ConfigProvider.boot(path.join(__dirname, 'configs'));

    expect(ConfigProvider.config.app).deep.eq(appConfig);
  });
});

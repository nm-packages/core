import 'boot';
import { expect } from 'chai';
import * as path from 'path';
import { ObjectId } from 'mongodb';
// provider
import DBProvider from 'providers/db.provider';
// models
import Test from './models/test.model';

describe('DB Provider', () => {
  before(async () => {
    await DBProvider.boot({
      connection: process.env.DATABASE_CONNECTION,
      options: '',
      modelsDir: path.join(__dirname, 'models'),
      name: 'amazon_manager_test',
    });
  });

  afterEach(async () => {
    await Test.collection.deleteMany({});
  });

  describe('Validate', () => {
    it('validate need required', async () => {
      let isCatchedError = false;

      try {
        const result = await Test.validate({});
      } catch {
        isCatchedError = true;
      }

      expect(isCatchedError).is.true;
    });

    it('validate other fields', async () => {
      const data = {
        first: 'test first',
        second: 'test second',
        third: '5f79beca34c3a4ace4dfff58',
        fourth: 'test fourth',
      };

      const result = await Test.validate(data);

      expect(result.first).eq(data.first);
      expect(result.second).eq(data.second);
      expect(result.third).eq(new ObjectId(data.third));
      expect(result.fourth).is.undefined;
      expect(result.default).eq('default');
    });
  });

  it('create', async () => {
    const data = {
      first: 'test first',
      second: 'test second',
    };

    const result = await Test.create(data);

    expect(result.first).eq(data.first);
    expect(result.second).eq(data.second);
    expect(result.created).is.not.undefined;
    expect(result.updated).is.not.undefined;
  });

  it('update', async () => {
    const createData = {
      first: 'test first',
    };

    const updateData = {
      first: 'update test',
      second: 'update test',
    };

    const createResult = await Test.create(createData);
    const updateResult = await Test.update(createResult._id, updateData);

    expect(updateResult._id).deep.eq(createResult._id);
    expect(updateResult.first).eq(updateData.first);
    expect(updateResult.second).eq(updateData.second);
    expect(updateResult.created).eq(createResult.created);
    expect(updateResult.updated).not.eq(createResult.updated);
  });

  it('get', async () => {
    const data = {
      first: 'test first',
      second: 'test second',
    };

    const createdResult = await Test.create(data);
    const getResult = await Test.get(createdResult._id);

    expect(createdResult).deep.eq(getResult);
  });

  it('list', async () => {
    const data = {
      first: new ObjectId().toString(),
    };

    const createdData = await Promise.all([
      Test.create(data),
      Test.create(data),
      Test.create(data),
    ]);

    const list = await Test.list({
      first: data.first,
    });

    expect(list.data).length(createdData.length);
  });

  it('pagination', async () => {
    const limit = 2;

    await Test.create({
      first: '1'
    });
    await Test.create({
      first: '2'
    });

    await Test.create({
      first: '3'
    });
    await Test.create({
      first: '4'
    });

    await Test.create({
      first: '5'
    });
    await Test.create({
      first: '6'
    });

    await Test.create({
      first: '7'
    });

    // next

    let list = await Test.list({}, {
      limit
    });

    expect(list.data).length(2);
    expect(list.hasNext).eq(true);
    expect(list.hasPrevious).eq(false);
    expect(list.next).eq(2);
    expect(list.previous).eq(null);
    expect(list.data[0].first).eq('1');
    expect(list.data[1].first).eq('2');

    list = await Test.list({}, {
      limit,
      next: list.next
    });

    expect(list.data).length(2);
    expect(list.hasNext).eq(true);
    expect(list.hasPrevious).eq(true);
    expect(list.next).eq(4);
    expect(list.previous).eq(0);
    expect(list.data[0].first).eq('3');
    expect(list.data[1].first).eq('4');

    list = await Test.list({}, {
      limit,
      next: list.next
    });

    expect(list.data).length(2);
    expect(list.hasNext).eq(true);
    expect(list.hasPrevious).eq(true);
    expect(list.next).eq(6);
    expect(list.previous).eq(2);
    expect(list.data[0].first).eq('5');
    expect(list.data[1].first).eq('6');

    list = await Test.list({}, {
      limit,
      next: list.next
    });

    expect(list.data).length(1);
    expect(list.hasNext).eq(false);
    expect(list.hasPrevious).eq(true);
    expect(list.next).eq(null);
    expect(list.previous).eq(4);
    expect(list.data[0].first).eq('7');

    // previous

    list = await Test.list({}, {
      limit,
      previous: list.previous
    });

    expect(list.data).length(2);
    expect(list.hasNext).eq(true);
    expect(list.hasPrevious).eq(true);
    expect(list.next).eq(6);
    expect(list.previous).eq(2);
    expect(list.data[0].first).eq('5');
    expect(list.data[1].first).eq('6');

    list = await Test.list({}, {
      limit,
      previous: list.previous
    });

    expect(list.data).length(2);
    expect(list.hasNext).eq(true);
    expect(list.hasPrevious).eq(true);
    expect(list.next).eq(4);
    expect(list.previous).eq(0);
    expect(list.data[0].first).eq('3');
    expect(list.data[1].first).eq('4');

    list = await Test.list({}, {
      limit,
      previous: list.previous
    });

    expect(list.data).length(2);
    expect(list.hasNext).eq(true);
    expect(list.hasPrevious).eq(false);
    expect(list.next).eq(2);
    expect(list.previous).eq(null);
    expect(list.data[0].first).eq('1');
    expect(list.data[1].first).eq('2');
  });

});

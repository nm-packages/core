import BaseModel from 'model';
import Schemes, { Default } from 'schemes';
// types
import { IModel } from 'types/model';

export interface ITestModel extends IModel {
  first: string;
  second?: string;
  third: any;
  default: string;
}

export class Test extends BaseModel<ITestModel> {
  protected collectionName = 'test';
  protected schema = {
    type: 'object',
    required: ['first'],
    properties: {
      first: Schemes.String,
      second: Schemes.String,
      third: Schemes.ObjectID,
      default: Default(Schemes.String, 'default')
    },
  };
}

export default new Test();

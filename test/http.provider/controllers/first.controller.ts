// decorators
import { get, route } from 'decorators/router.decorator';

@route('/first')
export default class AppController {
  @get('/test')
  public static async test() {
    return 'OK';
  }
}

// decorators
import { get } from 'decorators/router.decorator';

export default class AppController {
  @get('/test')
  public static async test() {
    return 'OK';
  }
}

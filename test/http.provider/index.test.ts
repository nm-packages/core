import { expect } from 'chai';
import * as path from 'path';
// provider
import HttpProvider from 'providers/http.provider';

describe('Http Provider', () => {
  it('load controllers and routings', async () => {
    await HttpProvider.boot({
      logger: false,
      port: 3000,
      controllersDir: path.join(__dirname, 'controllers'),
    });

    const [resFirst, resSecond] = await Promise.all([
      HttpProvider.request('/test'),
      HttpProvider.request('/first/test'),
    ]);

    expect(resFirst.body).eq('OK');
    expect(resSecond.body).eq('OK');
  });
});
